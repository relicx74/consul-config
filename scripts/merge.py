import sys
import logging
import inspect
import yaml #dependency on pyyaml 
import hiyapyco # (pip install hiyapyco, jinja2, pyyaml)

logging.basicConfig()

if len(sys.argv) < 4: 
   print "Usage: python merge.py [input folder] [input file base name] [outputfolder]"
   print "Example: python merge.py .. application ./out"
   sys.exit()

inputFolder=sys.argv[1]
inputBaseName = sys.argv[2]
outputFolder = sys.argv[3]
#verify output folder exists

envs = ["dev1", "dev2", "dev3", "qa1", "qa2", "qa3", "stag1", "stag2","stag3","stag4","prod1","prod2","prod3","prod4","prod5","prod6","prod7","prod8"] 
sites = ["ftd", "proflowers"]

#  for each environment
#  for each env/site merge application, application-[site], application-[env], application-[env]-[site] (rightmost override wins)
for env in envs:
   envYaml = dict()
   for site in sites:
      file1 = inputFolder + '/' + inputBaseName + '.yml'
      file2 = inputFolder + '/' + inputBaseName + '-' + site + '.yml'
      file3 = inputFolder + '/' + inputBaseName + '-' + env + '.yml'
      file4 = inputFolder + '/' + inputBaseName + '-' + env + '-' + site + '.yml'
      envYaml[site] = hiyapyco.load([file1, file2, file3, file4],  method=hiyapyco.METHOD_SIMPLE, failonmissingfiles=False, mergelists=False, loglevelmissingfiles=logging.INFO)
   #write out application-[env].yml once we have all the sites for this env
   with open(outputFolder + '/' + inputBaseName + '-' + env + '.yml', 'w') as file:
      file.write(hiyapyco.dump(envYaml))     
